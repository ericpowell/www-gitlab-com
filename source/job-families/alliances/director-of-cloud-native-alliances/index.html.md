---
layout: job_family_page
title: "Director of Cloud Native Alliances"
---

## Director of Cloud Native Alliances

### Responsibilities

* Create awareness for GitLab in cloud native circles (presentations, forums, blog posts, 1-1 interaction)
* Lead product integrations with Cloud Native technologies (Prometheus, Jaeger, etc.)
* Be the technical lead for public cloud partnerships in collaboration with the VP of Alliances.
* Educate the CEO and rest of the company on the up and coming cloud native technologies, organizations, people, and alliances.
* Lead corporate development of cloud native technology (talent and technology acquisitions)

### Requirements

* Aware of the cloud native trends, technologies, companies, alliances, and people.
* Based in the city of San Francisco
* History of public speaking at cloud native events
* Coordinate cross-functional work (Product, Engineering, Marketing, Sales, Finance, PeopleOps)
* Extensive experience with cloud native technologies
* Deep understanding of open source go to market and business models
